"use strict";
document.addEventListener("keydown", (event) => {
  let button = document.querySelectorAll(".btn");
  for (let i = 0; i < button.length; i++) {
    if (event.key.toUpperCase() === button[i].textContent.toUpperCase()) {
      button[i].style.backgroundColor = "blue";
    } else {
      button[i].style.backgroundColor = "black";
    }
  }
});
